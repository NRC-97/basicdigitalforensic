package application;
	
import java.io.File;
import java.io.IOException;
import java.util.Optional;

//Java Program to create fileChooser 
//and add it to the stage 
import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Stage; 

public class Main extends Application { 

public void start(Stage stage) 
{ 

 try { 

     // set title for the stage 
     stage.setTitle("FileChooser"); 

     // create a File chooser 
     FileChooser fil_chooser = new FileChooser();

     // create a Label 
     Label label = new Label("no files selected"); 
     
     // create a Button 
     Button button = new Button("Show open dialog"); 
     
     ImageView img = new ImageView();
     
     // create an Event Handler 
     EventHandler<ActionEvent> event =  
     new EventHandler<ActionEvent>() { 

         public void handle(ActionEvent e) 
         { 

             // get the file selected 
             File file = fil_chooser.showOpenDialog(stage); 
             //List<File> list = fil_chooser.showOpenMultipleDialog(stage);
             String liste = "";
             if (file != null) { 
//                   for (File file2 : list) {
//					liste += file2.getAbsolutePath() + " ; ";
//				}
                 label.setText(file.getAbsolutePath() + "  selected");
                   label.setText(liste);
                   Image image = new Image("file:" + file.getAbsolutePath(), 500, 500, true, true);
                   System.out.println(file.getName() + " " + file.length() + " " + file.canExecute() + " " + file.canRead() + " " + file.canWrite());
                   
                   img.setImage(image);
                   
                   Process p = null;
	   				try {
	   					p = Runtime.getRuntime().exec("ls -l " + file.getAbsolutePath());
	   				} catch (IOException e1) {
	   					// TODO Auto-generated catch block
	   					e1.printStackTrace();
	   				}
                    System.out.println(p);
                   
             } 
         } 
     };
     

     button.setOnAction(event); 

     // create a Button 
     Button button1 = new Button("Show save dialog"); 

     // create an Event Handler 
     EventHandler<ActionEvent> event1 =  
      new EventHandler<ActionEvent>() { 

         public void handle(ActionEvent e) 
         { 

             // get the file selected 
             File file = fil_chooser.showSaveDialog(stage); 

             if (file != null) { 
                 label.setText(file.getAbsolutePath()  
                                     + "  selected");
             } 
         } 
     }; 
     
     ProcessHandle.allProcesses()
     .forEach(process -> System.out.println(processDetails(process)));

     button1.setOnAction(event1); 
     
     final ToggleGroup group = new ToggleGroup();

     RadioButton rb1 = new RadioButton("Home");
     rb1.setToggleGroup(group);
     rb1.setSelected(true);

     RadioButton rb2 = new RadioButton("Calendar");
     rb2.setToggleGroup(group);
      
     RadioButton rb3 = new RadioButton("Contacts");
     rb3.setToggleGroup(group);
     
     rb1.setUserData("Home");
     rb2.setUserData("Calendar");
     rb3.setUserData("Contacts");

     group.selectedToggleProperty().addListener(new ChangeListener<Toggle>(){
         public void changed(ObservableValue<? extends Toggle> ov,
             Toggle old_toggle, Toggle new_toggle) {
                 if (group.getSelectedToggle() != null) {
//                     final Image image = new Image(
//                         getClass().getResourceAsStream(
//                             group.getSelectedToggle().getUserData().toString() + 
//                                 ".jpg"
//                         )
//                     );
//                     icon.setImage(image);
                	 
                	 System.out.println(group.getSelectedToggle().toString());
                 }                
             }
     });
     

     // create a VBox 
     VBox vbox = new VBox(30, img, label, button, button1, rb1, rb2, rb3); 

     // set Alignment 
     vbox.setAlignment(Pos.CENTER); 

     // create a scene 
     Scene scene = new Scene(vbox, 800, 500); 

     // set the scene 
     stage.setScene(scene); 

     stage.show(); 
 } 

 catch (Exception e) { 

     System.out.println(e.getMessage()); 
 } 
}

private static String processDetails(ProcessHandle process) {
    return String.format("%8d %8s %10s %26s %-40s",
            process.pid(),
            text(process.parent().map(ProcessHandle::pid)),
            text(process.info().user()),
            text(process.info().startInstant()),
            text(process.info().commandLine()));
}

private static String text(Optional<?> optional) {
    return optional.map(Object::toString).orElse("-");
}

	//Main Method 
	public static void main(String args[]) 
	{ 
	
	 // launch the application 
	 launch(args); 
	}

}	